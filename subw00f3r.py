import requests
from bs4 import BeautifulSoup

header = '''

           _              ___   ___   __ _____      
 ___ _   _| |____      __/ _ \ / _ \ / _|___ / _ __ 
/ __| | | | '_ \ \ /\ / / | | | | | | |_  |_ \| '__|
\__ \ |_| | |_) \ V  V /| |_| | |_| |  _|___) | |   
|___/\__,_|_.__/ \_/\_/  \___/ \___/|_| |____/|_|   
                                                   
Subdomain Finder
By: R4v3N - ms08067xp@gmail.com
'''

print header


subdomains = []
myvar = '/domain/'
relist = []
domain = raw_input('[*] Enter Domain: ')
r = requests.get('https://securitytrails.com/list/apex_domain/'+domain)
data = r.text
soup = BeautifulSoup(data, features='lxml')

for link in soup.find_all('a'):
	subdomains.append(link.get('href'))

relist = [i for i in subdomains if i]
subdomains[:] = []
for word in relist:
	if word.startswith(myvar):
		subdomains.append(word.split("/")[2])
subdomains = sorted(set(subdomains))

count = len(subdomains)
print '[*] Found ' +str(count)+' Possible Subdomains..+\n'
for item in subdomains:
	print item
